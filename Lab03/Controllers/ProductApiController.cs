﻿using Lab03.Models;
using Lab03.Repositories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Query.Internal;

namespace Lab03.Controllers
{
    [ApiController]
    [Route("api/products")]
    public class ProductApiController : ControllerBase
    {
        private readonly IproductRepository _productRepository;
        public ProductApiController(IproductRepository productRepository)
        {
            _productRepository = productRepository;
        }
        [HttpGet]
        public async Task<IActionResult> GetProducts()
        {
            try
            {
                var products = await _productRepository.GetProductAsync();
                return Ok(products);
            }
            catch (Exception ex)
            {
                //handle exception
                return StatusCode(500, "Inter server error");
            }
        }
        [HttpGet("{id}")]
        public async Task<IActionResult> GetByIdAsync(int id)
        {
            try
            {
                var product = await _productRepository.GetProductByIdAsync(id);
                if (product == null)
                {
                    return NotFound();

                }
                return Ok(product);
            }
            catch (Exception ex)
            {
                //Handle exception
                return StatusCode(500, "Internal server error");
            }
        }
        [HttpPost]
        public async Task<IActionResult> AddProduct([FromBody] Product product)
        {
            try
            {
                await _productRepository.AddAsync(product);
                return CreatedAtAction(nameof(GetByIdAsync), new { id = product.Id }, product);
            }
            catch (Exception ex)
            {
                //Handle exception
                return StatusCode(500, "Internal server error");
            }
        }
        [HttpPut("{id}")]
        public async Task<IActionResult>UpdateProduct(int id, [FromBody] Product product)
        {
            try
            {
                if (id != product.Id)
                    return BadRequest();
                await _productRepository.UpdateAsync(product);
                return NoContent();
            }
            catch(Exception ex)
            {
                //Handle exception
                return StatusCode(500, "Internal server error");
            }
            
        }
        [HttpDelete("{id}")]
        public async Task<IActionResult>DeleteProduct(int id)
        {
            try
            {
                await _productRepository.DeleteAsync(id);
                return NoContent();
            }
            catch(Exception ex)
            {
                //Handle exception
                return StatusCode(500, "Internal server error");
            }
        }
    }
}
