﻿using Lab03.Models;

namespace Lab03.Repositories
{
    public interface IproductRepository
    {
        Task<IEnumerable<Product>> GetProductAsync();
        Task<Product> GetProductByIdAsync(int id);
        Task AddAsync(Product product);
        Task UpdateAsync(Product product);
        Task DeleteAsync(int id);
    }
}
